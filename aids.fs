#
# Copyright (C) 2017 The OmniRom Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# /* SONY idd */
[AID_IDD]
value: 5987

# /* SONY updatemiscta */
[AID_UPDATEMISCTA]
value: 5991

# /* SONY trimarea */
[AID_TRIMAREA]
value: 5993

# /* SONY credmgr_client */
[AID_CREDMGR_CLIENT]
value: 5996

# /* SONY tad */
[AID_TAD]
value: 5997

# /* SONY ta_qmi */
[AID_TA_QMI]
value: 5998

[AID_WHATEVER]
value: 5999
